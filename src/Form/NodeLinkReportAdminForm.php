<?php

namespace Drupal\node_link_report\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class NodeLinkReportAdminForm.
 */
class NodeLinkReportAdminForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_link_report_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('node_link_report.settings');

    $form['enable_check_external_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable checking and reporting of external links'),
      '#description' => $this->t(
        'When enabled, the block will report on external links as well as internal links.'
      ),
      '#weight' => '0',
      '#default_value' => $config->get('enable_check_external_links'),
    ];

    $form['enable_reporting_good_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable reporting of good links'),
      '#description' => $this->t(
        'When enabled, the block will report good links as well as bad. Useful for seeing all links on the page.'
      ),
      '#weight' => '1',
      '#default_value' => $config->get('enable_reporting_good_links'),
    ];

    $form['enable_reporting_skipped_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable reporting of links not processed (skipped)'),
      '#description' => $this->t(
        'When enabled, the block will report any links that were skipped either because they designated to be skipped or can not be tested (mailto, tel, sms).'
      ),
      '#weight' => '2',
      '#default_value' => $config->get('enable_reporting_skipped_links'),
    ];

    $form['enable_reporting_inaccessible_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable reporting of accessibility issues with links'),
      '#description' => $this->t(
        'When enabled, the block will report any accessibility issues related to links.'
      ),
      '#weight' => '3',
      '#default_value' => $config->get('enable_reporting_inaccessible_links'),
    ];

    $form['accessibility_guidance_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Set a URL to use for accessibility guidance'),
      '#description' => $this->t('Leave this blank to have it not display. example:@url', ['@url' => 'https://www.w3.org/WAI/tips/writing/#make-link-text-meaningful']
      ),
      '#weight' => '4',
      '#default_value' => $config->get('accessibility_guidance_url'),
      '#size' => 60,
      '#maxlength' => 128,
      '#states' => [
        'visible' => [
          ':input[name="enable_reporting_inaccessible_links"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['accessibility_guidance_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The text to use in the guidance link'),
      '#description' => $this->t('Leave this blank to have it not display. example:Recommendations for making good accessible links.'),
      '#weight' => '5',
      '#default_value' => $config->get('accessibility_guidance_link'),
      '#size' => 60,
      '#maxlength' => 128,
      '#states' => [
        'visible' => [
          ':input[name="enable_reporting_inaccessible_links"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $defaults_display_on = [];
    if ($config->get('display_on_node_edit')) {
      $defaults_display_on[] = 'node_edit';
    }
    if ($config->get('display_on_node_view')) {
      $defaults_display_on[] = 'node_view';
    }
    if ($config->get('display_on_node_preview')) {
      $defaults_display_on[] = 'node_preview';
    }
    $form['allow_display_on'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('What pages should be able to display the link report block:'),
      '#description' => $this->t(
        'Regardless of these settings, the block will still need to placed in your desired region in the block admin (/admin/structure/block).'
      ),
      '#options' => [
        'node_view' => $this->t('Node View'),
        'node_edit' => $this->t('Node Edit'),
        'node_preview' => $this->t('Node Preview'),

      ],
      '#weight' => '6',
      '#default_value' => $defaults_display_on,
    ];

    $user_agent = (empty($config->get('user_agent'))) ? 'Drupal:Node Link Report link_checker' : $config->get('user_agent');
    $form['user_agent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Set the user agent for the curl requests that check the status of the links.'),
      '#description' => $this->t('This gives you the option to define the user agent for curl requests that check the links. Default: @name', [
        '@name' => 'Drupal:Node Link Report link_checker',
      ]),
      '#weight' => '8',
      '#default_value' => $user_agent,
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['additional_domains_as_internal'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Additional domains to treat as internal.'),
      '#description' => $this->t('If your site has multiple domains, add domains, one per line that can be treated as internal links. @site_example', [
        '@site_example' => '(www.example.com)',
      ]),
      '#weight' => '10',
      '#default_value' => $config->get('additional_domains_as_internal'),
      '#rows' => 10,
      '#cols' => 60,
      '#resizeable' => 'both',
    ];

    $form['decoupled_frontend'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alternate domain or subdomain of the frontend.'),
      '#description' => $this->t('If the frontend domain/subdomain for this website is different than that of the backend, enter the frontend domain. Example: @site_example', [
        '@site_example' => '(https://www.example.com)',
      ]),
      '#weight' => '12',
      '#default_value' => $config->get('decoupled_frontend'),
      '#size' => 60,
      '#maxlength' => 260,
    ];

    $form['domains_to_skip'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Domains that should be exempt from processing.'),
      '#description' => $this->t('If there are domains that are restricted access or other that incorrectly show as broken, add domains, one per line that should be skipped from processing. @site_example', [
        '@site_example' => '(www.example.com)',
      ]),
      '#weight' => '15',
      '#default_value' => $config->get('domains_to_skip'),
      '#rows' => 10,
      '#cols' => 60,
      '#resizeable' => 'both',
    ];

    $form['path_patterns_to_skip'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Paths that should be exempt from processing.'),
      '#description' => $this->t('If there are internal paths that should be skipped, you can add them here, one per line. @site_example', [
        '@site_example' => '(/node/*/edit, /user/*, /some/specific/path)',
      ]),
      '#weight' => '12',
      '#default_value' => $config->get('path_patterns_to_skip'),
      '#rows' => 10,
      '#cols' => 60,
      '#resizeable' => 'both',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => '20',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = self::configFactory()->getEditable('node_link_report.settings');
    $display_on = $form_state->getValue('allow_display_on');
    $config
      ->set('enable_check_external_links', $form_state->getValue('enable_check_external_links'))
      ->set('enable_reporting_inaccessible_links', $form_state->getValue('enable_reporting_inaccessible_links'))
      ->set('accessibility_guidance_url', $form_state->getValue('accessibility_guidance_url'))
      ->set('accessibility_guidance_link', $form_state->getValue('accessibility_guidance_link'))
      ->set('enable_reporting_good_links', $form_state->getValue('enable_reporting_good_links'))
      ->set('enable_reporting_skipped_links', $form_state->getValue('enable_reporting_skipped_links'))
      ->set('display_on_node_view', $display_on['node_view'])
      ->set('display_on_node_edit', $display_on['node_edit'])
      ->set('display_on_node_preview', $display_on['node_preview'])
      ->set('decoupled_frontend', $form_state->getValue('decoupled_frontend'))
      ->set('user_agent', $form_state->getValue('user_agent'))
      ->set('additional_domains_as_internal', $this->convertSort($form_state->getValue('additional_domains_as_internal')))
      ->set('domains_to_skip', $this->convertSort($form_state->getValue('domains_to_skip')))
      ->set('path_patterns_to_skip', $this->convertSort($form_state->getValue('path_patterns_to_skip')))
      ->save();

    // Since all report displays may have changed due to settings change,
    // invalidate all cache node link reports.
    Cache::invalidateTags(['node_link_report']);
  }

  /**
   * Dedupe, sort and convert to one per line.
   *
   * @param string $multivalues_raw
   *   A comma or new line separated string of multiple values.
   *
   * @return string
   *   A new-line separated string that has been deduped and sorted.
   */
  private function convertSort($multivalues_raw) {
    // Clean up any bad data by converting all separators into ','.
    $multivalues = str_replace(["\r\n", "\n", "\r", ', ', ' ,', " "], ',', $multivalues_raw);
    // Store the domains array as property.
    $values = explode(',', $multivalues);
    $values = array_unique($values);
    natcasesort($values);
    $multivalues_sorted = implode("\n", $values);
    return $multivalues_sorted;
  }

}
