<?php

namespace Drupal\node_link_report\Plugin\Block;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a Node Report Block.
 *
 * @Block(
 *   id = "node_link_report_block",
 *   admin_label = @Translation("Node Link Report"),
 * )
 */
class NodeLinkReport extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The scheme and host of the current site.
   *
   * @var string
   */
  private $currentSchemeAndHost;

  /**
   * Keyed by bad, good, skipped, and unpublished.
   *
   * @var array
   */
  private $links;

  /**
   * Configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Configuration settings object for node_link_report module.
   *
   * @var object
   */
  private $nlrConfig;

  /**
   * The node object for the current node.
   *
   * @var \Drupal\node\NodeInterface
   */
  private $node = NULL;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $routeMatch;

  /**
   * The cache backend interface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cacheBackend;

  /**
   * The HTTP request object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * Path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * Path validator interface.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * Node Link Report block construct.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend_interface
   *   The cache backend interface.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The HTTP request object.
   * @param \Drupal\Core\Renderer\RendererInterface $renderer_interface
   *   The renderer service.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager_interface
   *   Path alias manager.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator_interface
   *   Path validator interface.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match, CacheBackendInterface $cache_backend_interface, RequestStack $request_stack, RendererInterface $renderer_interface, AliasManagerInterface $alias_manager_interface, PathValidatorInterface $path_validator_interface) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->nlrConfig = $this->configFactory->get('node_link_report.settings');
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
    $this->cacheBackend = $cache_backend_interface;
    $this->requestStack = $request_stack;
    $this->renderer = $renderer_interface;
    $this->pathAliasManager = $alias_manager_interface;
    $this->pathValidator = $path_validator_interface;
    $this->links = [
      'bad' => [],
      'good' => [],
      'inaccessible' => [],
      'redirected' => [],
      'skipped' => [],
      'unpublished' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('cache.default'),
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('path_alias.manager'),
      $container->get('path.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $nid = $this->getNode()->id();
    $cid = "node_link_report_{$nid}";
    // Check for cached data.
    $cached_data = $this->cacheBackend->get($cid);
    if (!empty($cached_data) && (!$this->isPreview())) {
      $renderable = $cached_data->data;
    }
    else {
      // Cache unavailable so generate the data.
      $this->checkLinks();
      $renderable = [
        '#theme' => 'node_link_report_block',
        '#node_id' => $nid,
        '#bad_links' => $this->links['bad'],
        '#display_inaccessible_links' => $this->nlrConfig->get('enable_reporting_inaccessible_links'),
        '#inaccessible_links' => $this->links['inaccessible'],
        '#accessibility_guidance_url'  => $this->nlrConfig->get('accessibility_guidance_url'),
        '#accessibility_guidance_link' => $this->nlrConfig->get('accessibility_guidance_link'),
        '#display_good_links' => $this->nlrConfig->get('enable_reporting_good_links'),
        '#good_links' => $this->links['good'],
        '#display_skipped_links' => $this->nlrConfig->get('enable_reporting_skipped_links'),
        '#skipped_links' => $this->links['skipped'],
        '#redirected_links' => $this->links['redirected'],
        '#unpublished_links' => $this->links['unpublished'],
        '#attached' => [
          'library' => [
            'node_link_report/node-link-report',
          ],
        ],
      ];

      if (!$this->isPreview()) {
        // This is not a preview, so cache the data. for 24 hours.
        $expire_time = time() + 24 * 60 * 60;
        $tags = [
          'node_link_report',
          "node:{$nid}",
        ];
        $this->cacheBackend->set($cid, $renderable, $expire_time, $tags);
      }
    }

    return [
      '#markup' => $this->renderer->render($renderable),
    ];
  }

  /**
   * Checks the links of any found in the node content.
   *
   * @return array
   *   Array with the following keys 'bad', 'good', and 'unpublished'.
   */
  private function checkLinks() {
    $node = clone $this->getNode();
    $html = $this->getNodeHtml($node);
    // Instantiate the DOMDocument class.
    $htmlDom = new \DOMDocument();
    // Parse the HTML of the page using DOMDocument::loadHTML.
    @$htmlDom->loadHTML($html);

    // Extract the links from the HTML.
    $urls = $htmlDom->getElementsByTagName('a');
    $this->checkAccessibility($urls);
    // Dedupe the urls, by url.
    $deduped_urls = $this->dedupeUrls($urls);
    $curlable_urls = $this->obtainCurlableUrls($deduped_urls);
    $this->removeSelf($curlable_urls);
    $this->removeUrlsWithSkippedPaths($curlable_urls);
    // Process the curlable urls.
    $curl_fails = $this->curlThemAll($curlable_urls);

    foreach ($curl_fails as $url => $text) {
      // The url is not available and is either bad or unpublished.
      if ($this->isExternal($url)) {
        // This is definitely external and bad.
        $this->links['bad'][$url] = $text;
      }
      else {
        // The internal link may be bad or unpublished.
        if ($this->isUnpublished($url)) {
          // It is an url to an unpublished page.
          $this->links['unpublished'][$url] = $text;
        }
        else {
          // May be a page not known to drupal on decoupled site.
          if ($this->isOnFrontEnd($url)) {
            // Page exists on the frontend.
            $frontend_msg = $this->t('Found on frontend.');
            $this->links['good'][$url] = "{$text} *{$frontend_msg}";
          }
          else {
            // It is not unpublished, just bad.
            $this->links['bad'][$url] = $text;
          }
        }
      }
    }

    return $this->links;
  }

  /**
   * Checks all urls to identify accessiblity issues.
   *
   * This does not remove urls as inaccessble links will still be eligible
   * for testing.
   *
   * @param mixed $anchors
   *   All the anchors on the page as DOM objects.
   */
  protected function checkAccessibility($anchors) {
    foreach ($anchors as $key => $anchor) {
      $alt_text = $this->filterToDiscernableText($this->getImageAlt($anchor));
      $anchor_text = $this->filterToDiscernableText($anchor->nodeValue);
      $aria_label_text = $this->filterToDiscernableText($anchor->getAttribute('aria-label'));
      $href = $anchor->getAttribute('href');
      $anchor_meld = $anchor_text ?: $alt_text ?: $aria_label_text;
      // Case race.  First to eval TRUE wins.
      switch (TRUE) {
        case (empty($href) && empty($anchor_text) && !$this->hasImage($anchor)):
          // This is an allowable case for an anchor with an id. Do nothing.
          break;

        case (!empty($anchor_text) || !empty($alt_text) || !empty($aria_label_text)):
          $this->checkEmptyHref($href, $anchor_meld);
          // We have what is needed, so call it done.
          break;

        case (empty($anchor_text) && ($this->hasImage($anchor)) && empty($alt_text)):
          // There is only an image and it has no discernable alt.
          $this->checkEmptyHref($href, $anchor_meld);
          $linked_item = $this->getImageSrc($anchor);
          $raw_alt = $this->getImageAlt($anchor);
          if (!empty($raw_alt)) {
            $linked_item .= ' ' . $this->t("with alt '@raw_alt'.", ['@raw_alt' => $raw_alt]);
          }

          $destination = $anchor->getAttribute('href');
          $issue = $this->t("Image used in the link is missing meaningful alt text.");
          $how_to_fix = $this->t('Add alt text that indicates the destination of the link. For example, "Home page"');
          $this->makeAccessibilityMessage($key, $linked_item, 'image', $destination, $issue, $how_to_fix);

          break;

        case (empty($anchor_text) && empty($alt_text) && empty($aria_label_text)):
          // We have nothing so build the error msg.
          $this->checkEmptyHref($href, $anchor_meld);
          if (!$this->checkEmptyLinkText($anchor, $key)) {
            // It is not purely empty, so check to see if it is discernable.
            $this->checkDiscernable($anchor, $key);
          }
          break;

        default:
          break;
      }
    }
  }

  /**
   * Adds an array of an accessibility message data to the inaccessable msgs.
   *
   * @param int $key
   *   The index of the anchor on the page.
   * @param string $linked_item
   *   The anchor text or image being that needs the warning.
   * @param string $linked_item_type
   *   The type if linked item, usually 'text' or 'image'.
   * @param string $destination
   *   The href of the anchor.
   * @param string $issue
   *   An explanation of the issue.
   * @param mixed $how_to_fix
   *   The explanation of how to correct the issue.
   */
  protected function makeAccessibilityMessage($key, $linked_item, $linked_item_type, $destination, $issue, $how_to_fix) {
    $this->links['inaccessible']["link-$key"] = [
      'linked_item' => $linked_item,
      'linked_item_type' => $linked_item_type,
      'destination' => $destination,
      'issue' => $issue,
      'how_to_fix' => $how_to_fix,
    ];
  }

  /**
   * Creates a broken link warning if an href is empty.
   *
   * @param string $href
   *   The href to check.
   * @param string $anchor_text
   *   The anchor text to use in the warning if href is empty.
   */
  protected function checkEmptyHref($href, $anchor_text) {
    if (empty(trim($href))) {
      // Warn empty href.
      $this->links['bad'][$anchor_text] = $this->t("This link has no destination (href). Please remove the link or give it a destination.");
    }
  }

  /**
   * Check to see if an anchor has empty link text.
   *
   * @param object $anchor
   *   The anchor element from the DOM.
   * @param int $key
   *   The key for this anchor.
   *
   * @return bool
   *   TRUE if empty and not allowed. FALSE otherwise.
   */
  protected function checkEmptyLinkText($anchor, $key) {
    $text = trim($anchor->nodeValue);
    $href = $anchor->getAttribute('href');
    if (strlen($text) == 0) {
      if (empty($href)) {
        // An anchor with no href is not a link, so should be ignored.
        return FALSE;
      }
      // Us the full anchor since there is no link text to show.
      $linked_item = $anchor->C14N();
      $linked_item = utf8_decode($linked_item);
      $linked_item = "'{$linked_item}', {$this->getNearbyText($anchor)}";
      $issue = $this->t("The link has no text at all. It is not visible.");
      $how_to_fix = $this->t("Please remove this link or add text to the link so that readers understand where it will take them.");
      $this->makeAccessibilityMessage($key, $linked_item, 'text', $href, $issue, $how_to_fix);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Sets some empty text if there is no href.
   *
   * @param string $href
   *   An href value.
   *
   * @return string
   *   The original href or an empty message.
   */
  protected function fixHref($href) {
    if (empty($href)) {
      $href = $this->t('This link has no destination.');
    }
    return $href;
  }

  /**
   * Examine a link's surroundings and gather some context.
   *
   * @param object $anchor
   *   A anchor element from the DOM.
   *
   * @return string
   *   The locator string that helps provide context to where the link appears.
   */
  protected function getNearbyText($anchor) {
    $infront_of = $anchor->nextSibling;
    $comes_after = $anchor->previousSibling;
    $within = $anchor->parentNode;

    // Case race, first to evaluate TRUE wins.
    switch (TRUE) {
      case $infront_of !== NULL:
        // The link is in front of some text.
        $text = $infront_of->nodeValue;
        $text = $this->trimAndTruncate($text);
        $msg = $this->t("which comes before '@text'", ['@text' => $text]);
        break;

      case $comes_after !== NULL:
        // The link is in front of some text.
        $text = $comes_after->nodeValue;
        $text = $this->trimAndTruncate($text);
        $msg = $this->t("which comes after '@text'", ['@text' => $text]);
        break;

      case $within !== NULL:
        // The link is in front of some text.
        $text = $within->nodeValue;
        $text = $this->trimAndTruncate($text);
        $msg = $this->t("which comes within '@text'", ['@text' => $text]);
        break;

      default:
        $msg = $this->t("on this page");
        break;
    }

    return $msg;
  }

  /**
   * Dedupe urls.
   *
   * @param array|object $urls
   *   An array of url text keyed by url.
   *
   * @return array|object
   *   An array of text keyed by url.
   */
  private function dedupeUrls($urls) {
    $deduped_urls = [];
    foreach ($urls as $url) {
      // Get the link text.
      $linkText = $url->nodeValue;
      $linkText = utf8_decode($linkText);

      // Get the link in the href attribute.
      $linkHref = $url->getAttribute('href');
      if (!empty($linkHref)) {
        if (isset($deduped_urls[$linkHref])) {
          $deduped_urls[$linkHref] .= "[{$linkText}] ";
        }
        else {
          $deduped_urls[$linkHref] = "[{$linkText}] ";
          $deduped_urls[$linkHref] = "[{$linkText}] ";
        }
      }
    }

    return $deduped_urls;
  }

  /**
   * Checks to see if there is an image in the anchor.
   *
   * @param object $anchor
   *   The anchor element from the DOM.
   *
   * @return bool
   *   TRUE image is found. FALSE otherwise.
   */
  protected function hasImage($anchor) : bool {
    if (!empty($anchor->firstChild) && $anchor->firstChild->nodeName === 'img') {
      // We have an image.
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the img alt from an anchor with an image.
   *
   * @param object $anchor
   *   The anchor element from the DOM.
   *
   * @return string|bool
   *   String alt text if image is found. FALSE otherwise.
   */
  protected function getImageAlt($anchor) {
    if (!empty($anchor->firstChild) && $anchor->firstChild->nodeName === 'img') {
      // We have an image.
      $alt_text = $anchor->firstChild->getAttribute('alt');
      return $alt_text;
    }
    return FALSE;
  }

  /**
   * Get the img src from an anchor with an image.
   *
   * @param object $anchor
   *   The anchor element from the DOM.
   *
   * @return string|bool
   *   String if src image is found. FALSE otherwise.
   */
  protected function getImageSrc($anchor) {
    if (!empty($anchor->firstChild) && $anchor->firstChild->nodeName === 'img') {
      // We have an image.
      $src = $anchor->firstChild->getAttribute('src');

      return $src;
    }
    return FALSE;
  }

  /**
   * Checks to see if the page is available on the frontend location.
   *
   * @param string $url
   *   An internal url to lookup.
   *
   * @return bool
   *   TRUE if found on the frontend, FALSE if not.
   */
  private function isOnFrontEnd($url) {
    $frontend_domain = trim($this->nlrConfig->get('decoupled_frontend'));
    $parsed_url = parse_url($frontend_domain);
    if (!empty($frontend_domain) && !empty($parsed_url['scheme']) && !empty($parsed_url['host'])) {
      // There is a domain to work with.
      // Swap out the domain.
      $frontend_url = str_ireplace($this->getCurrentSchemeAndHost(), $frontend_domain, $url);
      $result = $this->curlUrl($frontend_url);
      if ($result['good']) {
        return TRUE;
      }
    }

    // There is no frontend domain to compare.
    return FALSE;
  }

  /**
   * Checks the anchor for discernable text and adds message if not found.
   *
   * @param mixed $anchor
   *   Anchor DOM object.
   * @param int $key
   *   The index of the anchor.
   */
  protected function checkDiscernable($anchor, $key) {

    if (empty($this->filterToDiscernableText($anchor->textContent))) {
      $linked_item = $anchor->nodeValue;
      $linked_item = utf8_decode($linked_item);
      $linked_item = "'{$linked_item}', {$this->getNearbyText($anchor)}";
      $destination = $anchor->getAttribute('href');
      $issue = $this->t("Text is not a descriptive word or phrase.");
      $how_to_fix = $this->t('Rewrite to help people understand where the link takes them. For example, "Home," or "Learn more about whales."');
      $this->makeAccessibilityMessage($key, $linked_item, 'text', $destination, $issue, $how_to_fix);
    };
  }

  /**
   * Remove non-discernable characters..
   *
   * @param string $text
   *   A string to analyse.
   *
   * @return string
   *   The text with all non discernable characters removed.
   */
  protected function filterToDiscernableText(string $text) {
    if ($text !== FALSE || $text !== NULL) {
      $non_discernable_chars = [
        // Forms of non-breaking spaces.
        '&nbsp;', "\xC2\xA0", '&#160;', '&#xa0;', "Â",
        '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '-', '+',
        '=', '{', '}', '[', ']', '|', ':', ';', '"', '?', '/', '>', '.', '<',
        '~', '`', "\r", "\n",
      ];
      $text = trim($text);
      $text = utf8_decode($text);
      $text = str_ireplace($non_discernable_chars, '', $text);
      $text = trim($text);
    }
    return $text;
  }

  /**
   * Remove the url of the current view of the page from the set of urls.
   *
   * @param array $urls
   *   By reference, an array of link text keyed by url.
   */
  private function removeSelf(array &$urls) {
    $node = $this->getNode();
    $urls_to_remove = [];
    $urls_to_remove['my_edit_url'] = $node->toUrl('edit-form')->setAbsolute()->toString();
    $urls_to_remove['my_path'] = str_replace('/edit', '', $urls_to_remove['my_edit_url']);
    $urls_to_remove['my_url'] = $node->toUrl()->setAbsolute()->toString();
    foreach ($urls_to_remove as $url_to_remove) {
      if (isset($urls[$url_to_remove])) {
        // A link to myself is present, so remove it.
        unset($urls[$url_to_remove]);
      }
    }
  }

  /**
   * Remove any urls that match the skip path.
   *
   * @param array $urls
   *   By reference, an array of link text keyed by url.
   */
  private function removeUrlsWithSkippedPaths(array &$urls) {
    $skip_patterns_raw = $this->nlrConfig->get('path_patterns_to_skip');
    // Clean up any bad data by converting all separators into ','.
    $skip_patterns_raw = str_replace(["\r\n", "\n", "\r", ', ', ' ,', " "], ',', $skip_patterns_raw);
    // Store the domains array as property.
    $skip_patterns = explode(',', $skip_patterns_raw);
    foreach ($urls as $url => $text) {
      // We are looking for pattern matches of internal domains.
      if (!$this->isExternal($url) && $this->isSkipPatternsMatch($url, $skip_patterns)) {
        $this->links['skipped'][$url] = $text;
        unset($urls[$url]);
      }
    }
  }

  /**
   * Checks if the url matches any of multiple patterns.
   *
   * @param string $url
   *   An url to compare to the patterns.
   * @param array $skip_patterns
   *   A flat array of patterns.
   *
   * @return bool
   *   TRUE if a match was found, FALSE if no match was found.
   */
  private function isSkipPatternsMatch($url, array $skip_patterns) {
    foreach ($skip_patterns as $skip_pattern) {
      if ($this->isSkipPatternMatch($url, $skip_pattern)) {
        // We have a match. Be done.
        return TRUE;
      }
    }
    // If we made it this far, there is no match.
    return FALSE;
  }

  /**
   * Checks if the url matches a single pattern.
   *
   * @param string $url
   *   An url to compare to the pattern.
   * @param string $skip_pattern
   *   A single path pattern to match.
   *
   * @return bool
   *   TRUE if it matches the pattern, FALSE if does not match the pattern.
   */
  private function isSkipPatternMatch($url, $skip_pattern) {
    if (!empty($skip_pattern)) {
      $path = parse_url($url, PHP_URL_PATH);
      $path = trim($path, "/ \t\n\r\0\x0B");
      $path_parts = explode('/', $path);
      $skip_pattern = trim($skip_pattern, "/ \t\n\r\0\x0B");
      $pattern_parts = explode('/', $skip_pattern);
      // If the last item in the pattern parts is not a * then counts must
      // match. Otherwise there would be children would be match by a matching
      // parent pattern.
      $last_pattern = end($pattern_parts);
      if (($last_pattern !== '*') && (count($pattern_parts) !== count($path_parts))) {
        // Last item is not a wildcard, so pattern and path counts don't match.
        return FALSE;
      }

      foreach ($pattern_parts as $index => $pattern_part) {
        if ($pattern_part === '*') {
          // This pattern part is a wildcard.  Automatic match, move on.
          continue;
        }
        elseif ($pattern_part !== $path_parts[$index]) {
          // This part did not match, so declare no match and call it off.
          return FALSE;
        }
      }
      // Made it this far, there must be a full match to the pattern.
      return TRUE;
    }
    // Made it to here, there was nothing to match.
    return FALSE;
  }

  /**
   * Determine what urls can be curled.
   *
   * @param array $urls
   *   An array of url text keyed by url.
   *
   * @return array
   *   An array of text keyed by url that can be curled.
   */
  private function obtainCurlableUrls(array $urls) {
    $curlable_urls = [];
    foreach ($urls as $url => $text) {
      if ($this->isAllowed($url, $text)) {
        $curlable_urls[$url] = $text;
      }
    }
    return $curlable_urls;
  }

  /**
   * Looks up to see if an internal link is unpublished.
   *
   * @param string $url
   *   A url to lookup.
   *
   * @return bool
   *   TRUE if the link points to an unpublished entity.  FALSE otherwise.
   */
  private function isUnpublished($url) {
    // Look at the path.
    $path = parse_url($url, PHP_URL_PATH);
    // Determine if it is a node/NID path.
    if (!(strpos($path, 'node') === 0) || !(strpos($path, 'taxonomy/term') === 0)) {
      // It is an alias, so convert it to a system path.
      $path = $this->pathAliasManager->getPathByAlias($path);
    }
    $is_valid = $this->pathValidator->isValid($path);
    if ($is_valid) {
      try {
        $params = Url::fromUri("internal:{$path}")->getRouteParameters();
        $entity_type = key($params);
        $entity = $this->entityTypeManager->getStorage($entity_type)->load($params[$entity_type]);
        if (($entity instanceof ContentEntityInterface) && (method_exists($entity, 'isPublished')) && (!$entity->isPublished())) {
          // It is an unpublished entity.
          return TRUE;
        }
        else {
          // It is something other than an unpublished entity.
          return FALSE;
        }
      }
      catch (\Throwable $e) {
        // Something went badly. Mark it as not unpublished.
        return FALSE;
      }

    }
    else {
      // Not a valid path.
      return FALSE;
    }
  }

  /**
   * Get the html from the node contents.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object to generate the html from.
   *
   * @return string
   *   The html of the node content.
   */
  private function getNodeHtml(NodeInterface $node) {
    $builder = $this->entityTypeManager->getViewBuilder('node');
    // Use a bogus view mode so that no view mode will be used.
    // This might be a mistake, but seems to be working.
    $view_mode = 'bogus-view-mode-12345-giberish';
    $build = $builder->view($node, $view_mode);
    $html = \Drupal::service('renderer')->render($build);

    return $html;
  }

  /**
   * Checks if this is node preview.
   *
   * @return bool
   *   TRUE if it is node preview.  FALSE otherwise.
   */
  private function isPreview() {
    $route_name = $this->routeMatch->getRouteName();
    return ($route_name === "entity.node.preview") ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    // Check to see if we are on node/NID. node/NID/edit or node preview.
    $route_name = $this->routeMatch->getRouteName();
    $allowed_routes = [];
    if ($this->nlrConfig->get('display_on_node_view')) {
      $allowed_routes[] = 'entity.node.canonical';
    }
    if ($this->nlrConfig->get('display_on_node_edit')) {
      $allowed_routes[] = 'entity.node.edit_form';
    }
    if ($this->nlrConfig->get('display_on_node_preview')) {
      $allowed_routes[] = 'entity.node.preview';
    }

    if (in_array($route_name, $allowed_routes)) {
      return AccessResult::allowedIfHasPermission($account, 'view node link report');
    }
    else {
      // Not on the right path, hide the block.
      return AccessResult::forbiddenIf(TRUE);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * Checks if the URL can be tested.
   *
   * @param string $url
   *   An url to be evaluated.  Relative urls will be converted to absolute.
   * @param string $text
   *   The text on the link.
   *
   * @return bool
   *   TRUE if the url should be tested.  FALSE otherwise.
   */
  private function isAllowed(&$url, $text) {
    $process_external = $this->nlrConfig->get('enable_check_external_links');
    // Fix parse_url bug where 'tel:nnnn' is not parsed correctly.
    // Get rid of space after`tel:` in case it exists.
    $url = str_replace('tel: ', 'tel:', $url);
    // Add space to make sure it exists.
    $url = str_replace('tel:', 'tel: ', $url);
    // Check for mailto, and other schemes that can not be curled.
    $scheme = parse_url($url, PHP_URL_SCHEME) ?? '';
    $scheme = strtolower($scheme);
    $non_curlable = ['mailto', 'im', 'sms', 'tel'];
    if (in_array($scheme, $non_curlable)) {
      // This is not curl-able but is a valid scheme, so skip it.
      $this->links['skipped'][$url] = $text;
      return FALSE;
    }
    $this->convertRelativeToFull($url);
    if ($this->isExternal($url)) {
      // Is external, see if we should process.
      if ($process_external) {
        if (in_array(parse_url($url, PHP_URL_HOST), $this->getDomains('domains_to_skip'))) {
          $this->links['skipped'][$url] = $text;
          return FALSE;
        }
        else {
          return TRUE;
        }
      }
      else {
        return FALSE;
      }
    }
    else {
      // Is internal.
      return TRUE;
    }
  }

  /**
   * Convert relative links to absolute links.
   *
   * @param string $url
   *   Url by reference to make a full url if it is a relative URL.
   */
  private function convertRelativeToFull(&$url) {
    // Make the url slashless for consistency.
    $url = ltrim($url, '/');
    $schema = parse_url($url, PHP_URL_SCHEME);
    if (empty($schema)) {
      // Assume this is relative url. (Some risk here.)
      $url = "{$this->getCurrentSchemeAndHost()}/{$url}";
    }
  }

  /**
   * Evaluates if a url is internal.
   *
   * @param string $url
   *   An URL to check.
   *
   * @return bool
   *   TRUE if the URL is external to the site.
   */
  private function isExternal($url) {
    $internal_domains = $this->getDomains('additional_domains_as_internal');
    // Add the actual host.
    $internal_domains[] = $this->requestStack->getCurrentrequest()->getHost();
    $url_host = parse_url($url, PHP_URL_HOST);
    return (in_array($url_host, $internal_domains)) ? FALSE : TRUE;
  }

  /**
   * Getter and setter for the current node.
   *
   * @return \Drupal\node\NodeInterface
   *   Returns the node for the current route. or NULL if not found.
   */
  protected function getNode() {
    if (empty($this->node)) {
      $route_name = $this->routeMatch->getRouteName();

      if (($route_name === 'entity.node.canonical') || ($route_name === 'entity.node.edit_form')) {
        $node = $this->routeMatch->getParameter('node');
      }
      elseif ($route_name === 'entity.node.preview') {
        $node = $this->routeMatch->getParameter('node_preview');
      }

      if ($node instanceof NodeInterface) {
        $this->node = $node;
      }
    }
    return $this->node;
  }

  /**
   * Getter and Setter for the Scheme and Host of this Drupal site.
   *
   * @return string
   *   The scheme and host of the current site. https://example.com
   */
  private function getCurrentSchemeAndHost() {
    if (empty($this->schemeAndHost)) {
      $this->currentSchemeAndHost = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    }
    return $this->currentSchemeAndHost;
  }

  /**
   * Getter and setter for domain storage.
   *
   * @param string $type
   *   The type of domain list storage.  'additional_domains_as_internal' or
   *   'domains_to_skip'.
   *
   * @return array
   *   An array from the domains property.
   */
  private function getDomains($type) {
    if (!isset($this->$type)) {
      // This has not been built yet, so build it.
      $domain_list = $this->nlrConfig->get($type);
      // Clean up any bad data by converting all separators into ','.
      $domain_list = str_replace(["\r\n", "\n", "\r", ', ', ' ,', " "], ',', $domain_list);
      // Clean up by removing any scheme.
      $domain_list = str_replace(['http://', 'https://'], '', $domain_list);
      // Store the domains array as property.
      $this->$type = explode(',', $domain_list);
    }
    // Return the domains array property.
    return $this->$type;
  }

  /**
   * Curls all urls and adds success to the links array and returns any fails.
   *
   * @param array $curlable_urls
   *   An array of text keyed by url.
   *
   * @return array
   *   An array of items that failed curl.  Items text keyed by url.
   */
  private function curlThemAll(array $curlable_urls) {
    $curl_fails = [];
    $mh = curl_multi_init();
    $curl_handles = $this->getCurlHandles($curlable_urls);
    $active = NULL;
    // Register all the handles.
    foreach ($curl_handles as $key => $ch) {
      curl_multi_add_handle($mh, $curl_handles[$key]);
    }

    // Make all the requests.
    do {
      $ret = curl_multi_exec($mh, $active);
      // Slow things down a wee bit to keep from using all the cycles.
      usleep(200000);
    } while ($ret == CURLM_CALL_MULTI_PERFORM);
    // Make sure all the requests complete.
    while ($active && $ret == CURLM_OK) {
      if (curl_multi_select($mh) != -1) {
        do {
          $mrc = curl_multi_exec($mh, $active);
          usleep(200000);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);
      }
    }
    // Process all the curl return codes.
    foreach ($curlable_urls as $url => $text) {
      $returncode = curl_getinfo($curl_handles[$url], CURLINFO_HTTP_CODE);
      $redirected = curl_getinfo($curl_handles[$url], CURLINFO_REDIRECT_COUNT);
      $destination = curl_getinfo($curl_handles[$url], CURLINFO_EFFECTIVE_URL);
      if (in_array($returncode, [400, 405, 501, 503])) {
        // This url not support header only requests.  Try full curl.
        $full_curl = $this->curlUrl($url, FALSE);
        $returncode = $full_curl['return_code'];
        $redirected = $full_curl['redirect_count'];
        $destination = $full_curl['effective_url'];
      }
      // We want to not call out redirected links that differ only by a
      // trailing slash.
      $differ_only_by_slash = rtrim($url, '/') === rtrim($destination, '/');
      $acceptable_return_codes = [200, 301, 302, 304, 307, 308];
      if (in_array($returncode, $acceptable_return_codes)) {
        // Curl was good so record it as good.
        if (($redirected > 0) && (!$differ_only_by_slash)) {
          // This was redirected to some place else, but ended ok.
          $this->links['redirected'][$url] = [
            'text' => $text,
            'destination' => $destination,
          ];
        }
        else {
          // This was a good link with no redirect.
          $this->links['good'][$url] = $text;
        }
      }
      else {
        // Curl was bad, so add it to the failed curl.
        $curl_fails[$url] = $text;
      }
    }
    curl_multi_close($mh);

    return $curl_fails;
  }

  /**
   * Curls a single url and returns TRUE if the url is found.
   *
   * @param string $url
   *   A url to curl.
   * @param bool $header_only
   *   Flag to determine if this should be a header only curl. (default TRUE).
   *
   * @return array
   *   Contains elements good, return_code, redirect_count, and effective_url.
   */
  private function curlUrl($url, $header_only = TRUE) {
    $return = [
      'good' => FALSE,
      'return_code' => NULL,
      'redirect_count' => NULL,
      'effective_url' => NULL,
    ];
    try {
      $ch = curl_init($url);
      if ($header_only) {
        // We just want the header.
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        // We don't need the body.
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);
      }
      // Catch output, do not print.
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      // Follow redirects.
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
      // Allow up to 10 redirects to not get stuck for too long.
      curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      // Prevent waiting forever to get a result.
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
      curl_setopt($ch, CURLOPT_TIMEOUT, 10);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0");
      curl_exec($ch);
      $return['return_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      $return['redirect_count'] = curl_getinfo($ch, CURLINFO_REDIRECT_COUNT);
      $return['effective_url'] = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);

      curl_close($ch);
      $acceptable_return_codes = [200, 301, 302, 307, 308];
      if (in_array($return['return_code'], $acceptable_return_codes)) {
        $return['good'] = TRUE;
      }
      return $return;
    }
    catch (\Exception $e) {
      watchdog_exception('node_link_report', $e);
      return $return;
    }
  }

  /**
   * Builds all the handles for all the urls.
   *
   * @param array $curlable_urls
   *   An array of text keyed by url.
   *
   * @return array
   *   An array of curl handled keyed by url.
   */
  private function getCurlHandles(array $curlable_urls) {
    $curl_handles = [];
    foreach ($curlable_urls as $url => $text) {
      $ch = curl_init($url);
      // We just want the header.
      curl_setopt($ch, CURLOPT_HEADER, TRUE);
      // We don't need the body.
      curl_setopt($ch, CURLOPT_NOBODY, TRUE);
      // Catch output, do not print.
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      // Follow redirects.
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
      // Allow up to 10 redirects to not get stuck for too long.
      curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      // Prevent waiting forever to get a result.
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
      curl_setopt($ch, CURLOPT_TIMEOUT, 10);
      $user_agent = (empty($this->nlrConfig->get('user_agent'))) ? 'Drupal:Node Link Report link_checker' : $this->nlrConfig->get('user_agent');
      curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
      $curl_handles[$url] = $ch;
    }

    return $curl_handles;
  }

  /**
   * Trim and truncate text in a consistent manner.
   *
   * @param string $text
   *   A text string to be truncated.
   *
   * @return string
   *   Truncated text.
   */
  protected function trimAndTruncate($text) {
    $text = trim($text);
    return mb_strimwidth($text, 0, 30, "...");
  }

}
